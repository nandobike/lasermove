/**
 * Simple Write. 
 *  Control of a stage with grbl
 */
import processing.serial.*;
Serial myPort;  // Create object from Serial class
int val;        // Data received from the serial port



int y_wait=100; //how many milliseconds to wait in each y-step. Increase this if you want movement in y-axis slower
float y_step=0.1; // how much to move in each y-step
float y_pos=0.0; //position in Y,

void setup() 
{
  size(200, 200);
  print( Serial.list());
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 115200);
  delay(2000);
}


void draw() {
  background(255);
    myPort.write("Y"+str(y_pos));
    myPort.write(10);
    println("Y"+str(y_pos)); //just print in the screen what position was sent
    delay(y_wait);
    y_pos-=y_step;
}
