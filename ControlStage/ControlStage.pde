/**
 * Simple Write. 
 *  Control of a stage with grbl
 */
import processing.serial.*;
Serial myPort;  // Create object from Serial class
int val;        // Data received from the serial port

float x=0.0; //x-position to be sent to controller
int delay_x=500; //time to wait after moving a step in the x-direction




int cycles=5; //total number of steps in the x-direction
float step_x=0.1; //size of the step in the x-direction
boolean treat=true; //the program will move the controller while treat is true

int z_wait=100; //how many milliseconds to wait in each z-step. Increase this if you want movement in z-axis slower
float z_step=0.1; // how much to move in each z-step
float z_pos=0.0; //position in Z,
float z_bottom=-10.0; //bottom position in z

void setup() 
{
  size(200, 200);
  print( Serial.list());
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 115200);
  delay(2000);
}


void draw() {
  background(255);
  if (treat==true) {
    for (int i = 1; i < cycles; i+=2) {
      x=i*step_x;
      godown();
      move_x(x); //this is a function that tells where to move the x-axis
      goup();
      x=(i+1)*step_x;
      move_x(x);     
    }
    treat=false; //this tells the program to stop moving the motors
  }
}

void godown() { //this routine moves the z-axis downwards
  while (z_pos > z_bottom) {
    myPort.write("Z"+str(z_pos));
    myPort.write(10);
    println("Z"+str(z_pos)); //just print in the screen what position was sent
    delay(z_wait);
    z_pos-=z_step;
  }
}

void goup() { //this routine moves the z-axis upwards
  while (z_pos < 0) {
    myPort.write("Z"+str(z_pos));
    myPort.write(10);
    println("Z"+str(z_pos)); //just print in the screen what position was sent
    delay(z_wait);
    z_pos+=z_step;
  }
}

void move_x(float position) {
      myPort.write("X"+str(position));
      myPort.write(10);
      println("X"+str(position));
      delay(delay_x);
}
